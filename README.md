# NetChris Front Desk

Head on over to [the wiki](https://gitlab.com/NetChris/front-desk/wikis) for the information this project is meant to provide!

Or, if you're just wanting to file an issue:

- <a href="https://gitlab.com/cssl/front-desk/issues/new" target="_blank">Do that here</a>
- [Or email one in](mailto:incoming+netchris-front-desk-10174612-qRLV9sgQGL3_vdydt7Kh-issue@incoming.gitlab.com)
